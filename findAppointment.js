let moment = require('moment');

const schedules = [
	[['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']],
	[['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']],
	[['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '19:00']]
];

const timeFormat = 'HH:mm'; // moment time format
const workStart = '09:00';
const workEnd = '19:00';
const duration = 60;

function findAppointment(schedules, duration) {

	let appointmentStart = workStart;
	let appointmentEnd = getAppointmentEnd(appointmentStart);

	function getAppointmentEnd(start) {
		return moment(start, timeFormat).add(duration, 'm').format(timeFormat)
	}

	// combine all schedules
	let schedule = [];
	for(let i in schedules) {
		schedule = schedule.concat(schedules[i]);
	}
	// sort schedule
	schedule.sort();

	for(let i in schedule) {
		let start = schedule[i][0];
		let end = schedule[i][1];

		// check if there is a schedule conflict with proposed appointment time
		if((appointmentStart >= start && appointmentStart < end) || (appointmentEnd >= start && appointmentEnd < end)) {
			appointmentStart = end;
			appointmentEnd = getAppointmentEnd(appointmentStart);
		} else {
			break;
		}
	}

	if(appointmentEnd > workEnd) {
		appointmentStart = null;
	}

	return appointmentStart;

}

let time = findAppointment(schedules, duration);
console.log(time);
