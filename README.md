### Appointments

Find earliest available appointment time, given schedules and appointment duration.

## Getting Started

* Install dependencies `npm install`
* Start `node findAppointment.js`
* Earliest appointment time will be printed out

## Built With

* [Moment.js](https://momentjs.com/)

## Author

* **Daniel Barrington**